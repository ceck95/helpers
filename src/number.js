const Hoek = require('hoek');

class NumberHelper {
  static randomNumber(opts) {
    if (opts) {
      if (opts.length) {
        const length = opts.length;
        Hoek.assert(length < 22, 'Max length < 22');
        let number = '';
        for (let i = 0; i < length - 1; i++) {
          number += '0';
        }
        return Math.floor(
          parseInt(`1${number}`) + Math.random() * parseInt(`9${number}`)
        );
      }
      if (opts.between) {
        const min = opts.between.min,
          max = opts.between.max;
        return Math.floor(Math.random() * (max - min + 1)) + min;
      }
    }
    return Math.random();
  }

  static isNumber(val, opts = {}) {
    return !isNaN(parseFloat(val)) && isFinite(val);
  }
}

module.exports = NumberHelper;
