class Joi {
  static joiToObject(schema) {
    const listKey = Object.keys(schema),
      current = {};
    listKey.forEach(e => {
      const currentType = schema[e]._type;
      if (currentType === 'object')
        current[e] = Joi.joiToObject(Joi.parseObjectJoi(schema[e]));
      else if (
        typeof schema[e] === 'object' &&
        schema[e] !== null &&
        !schema[e].isJoi
      )
        current[e] = Joi.joiToObject(schema[e]);
      else if (currentType === 'array')
        current[e] = Joi.parseArrayJoi(schema[e]);
      else current[e] = currentType;
    });
    return current;
  }

  static parseObjectJoi(objectJoi) {
    objectJoi = objectJoi._inner.children;
    const result = {};
    if (!objectJoi) return result;
    objectJoi.forEach(e => {
      result[e.key] = e.schema;
    });
    return result;
  }

  static parseArrayJoi(arrayJoi) {
    arrayJoi = arrayJoi._inner.items;
    const result = [];
    arrayJoi.forEach(e => {
      if (e._type === 'object') {
        result.push(Joi.joiToObject(Joi.parseObjectJoi(e)));
      } else {
        result.push(e._type);
      }
    });
    return result;
  }
}

module.exports = Joi;
