const config = require('config');
const BPromise = require('bluebird');
const rq = require('request-promise');

class OAuthHelpers {
  static request(req, data, opts = {}) {
    const { uri, body, method, baseUrl } = data;
    const methodInit = body ? 'POST' : 'GET';
    if (opts.basic) {
      const basicCfg = config.oAuth.basic.jwt;
      req.headers.authorization = `Basic ${new Buffer(
        `${basicCfg.clientSecret}.${basicCfg.clientId}`
      ).toString('base64')}`;
    }

    return new BPromise((resolve, reject) => {
      let cfgRq = {
        uri: uri,
        baseUrl: baseUrl || config.oAuth.baseUrl,
        method: method || methodInit,
        headers: {
          authorization: req.headers.authorization
        },
        json: true
      };
      if (data.paramUri) {
        const dataParams = data.paramUri;
        Object.keys(dataParams).forEach(e => {
          cfgRq.uri = cfgRq.uri.replace(`{${e}}`, dataParams[e]);
        });
      }
      if (body) {
        cfgRq.body = body;
      }

      return rq(cfgRq)
        .then(data => {
          if (opts.responseData) {
            return resolve(data.data);
          }
          return resolve(data);
        })
        .catch(error => {
          if (error.cause && error.cause.code === 'ECONNREFUSED') {
            return reject({
              code: '513',
              source: error.message
            });
          }
          error.error.errors[0].source = `Error from url: ${
            error.response.request.uri.href
          }`;
          return reject(error.error.errors);
        });
    });
  }
}

module.exports = OAuthHelpers;
