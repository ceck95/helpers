const helperJson = require('./json');

class RequestHelpers {
  static parseParamPaging(req, nameConfig) {
    const config = req[nameConfig || 'config'],
      query = req.query;
    return {
      pageSize: query.pageSize || config.pageSize,
      pageNumber: query.pageNumber || 1
    };
  }

  static parseParamFilter(req, filter, opts) {
    opts = opts || {};
    filter = filter || {};
    const query = helperJson.cloneDeep(req.query) || {},
      exceptDefault = ['pageSize', 'pageNumber'],
      except = Array.isArray(opts.except)
        ? exceptDefault.concat(opts.except)
        : exceptDefault;

    except.forEach(e => {
      if (query[e] || query[e] === null) {
        delete query[e];
      }
    });

    return {
      ...filter,
      ...query
    };
  }
}

module.exports = RequestHelpers;
