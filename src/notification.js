const Hoek = require('hoek');

class Notification {

  static loadNotification() {

    let notification = {};
    try {
      notification = require(`${process.cwd()}/resources/notifications/vn.json`);
    } catch (error) {
      //no handle
    }
    return notification;

  }

  static translateNotification(notification, opts = {}) {
    const notifications = Notification.loadNotification(),
      currentNotification = JSON.parse(JSON.stringify(notifications[notification.name]));
    Hoek.assert(currentNotification, `[Translate Notification] notification cannot found.Please check translate notification with name "${notification.name}"`);

    Object.keys(currentNotification).forEach(e => {
      const dataOpt = notification[e];
      if (typeof dataOpt === 'object' && dataOpt.params) {
        const params = dataOpt.params;
        Object.keys(params).forEach(a => {
          currentNotification[e] = currentNotification[e].replace(`{{${a}}}`, params[a]);
        });
      }
    });

    return currentNotification;
  }

}

module.exports = Notification;