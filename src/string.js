class Convert {
  static toSnakeCase(word, opts) {
    let delimiter = '_';
    if (opts && opts.delimiter) {
      delimiter = opts.delimiter;
    }
    const reg = new RegExp(`^${delimiter}`);
    return word
      .replace(/(?:^|\.?)([A-Z])/g, (x, y) => {
        return `${delimiter}${y.toLowerCase()}`;
      })
      .replace(reg, '');
  }

  static toCamelCase(word, opts) {
    let delimiter = '_';
    if (opts && opts.delimiter) {
      delimiter = opts.delimiter;
    }
    const regExp = new RegExp(`(${delimiter}[a-z])`, 'g');
    return word.replace(regExp, (x, y) => {
      return y.replace(delimiter, '').toUpperCase();
    });
  }

  static slug(slug, delimiter) {
    delimiter = delimiter || '-';
    slug = slug
      .replace(/[-!$%^&*()_+|~=`{}\[\]:";'<>?,.\/]/g, (x, y) => {
        return x.replace(x, delimiter);
      })
      .replace(/ /g, (x, y) => {
        return x.replace(x, delimiter);
      })
      .toLowerCase();

    slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
    slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
    slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
    slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
    slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
    slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
    slug = slug.replace(/đ/gi, 'd');

    slug = slug
      .replace(/-{2,}/g, (x, y) => {
        return x.replace(x, delimiter);
      })
      .replace(/^-{1,}/g, '')
      .replace(/-{1,}$/g, '')
      .replace(/-{2,}/g, delimiter);

    return slug;
  }

  static truncate(src, opts = {}) {
    const delimiter = opts.delimiter || ' ';
    src = src.trim();
    const splitSrc = src.split(delimiter, opts.cutPosition || 3);
    if (splitSrc.length === 1) return src;
    src = splitSrc.join(delimiter);
    return `${src.split(delimiter, opts.cutPosition || 3).join(delimiter)} ...`;
  }
}

module.exports = Convert;
