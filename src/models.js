class HelperModel {

  static json(data, key) {
    if (data[key]) {
      data[key] = JSON.parse(data[key]);
    }
    return data;
  }

  static jsonString(data, key) {
    if (data[key]) {
      data[key] = JSON.stringify(data[key]);
    }
    return data;
  }

}

module.exports = HelperModel;