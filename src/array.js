class Array {

  static uniqueItem(data) {
    return data.filter((value, index, array) => {
      return array.indexOf(value) === index;
    });
  }

}

module.exports = Array;