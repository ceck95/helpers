const Hoek = require('hoek');
const clone = require('clone');

class Json {

  static clone(obj, opts) {
    return JSON.parse(JSON.stringify(obj));
  }

  static cloneDeep(obj) {
    return clone(obj);
  }

  static checkEmptyObject(obj, opts) {
    return Object.keys(obj).length === 0;
  }

  static parse(src, opts) {
    Hoek.assert(typeof src === 'string', 'Data must string');
    let result;
    try {
      result = JSON.parse(src);
    } catch (err) {
      // eslint-disable-next-line no-console
      console.error(err.message);
      switch (err.message) {
        case 'Unexpected token ﻿ in JSON at position 0':
          result = JSON.parse(src.trim());
          break;
        default:
          result = JSON.parse(String.raw(`${src}`));
          break;
      }
    }
    return result;
  }


  static compareString(a, b, opts) {
    const covertToString = (s) => {
      if (typeof s !== 'string')
        s = JSON.stringify(s);
      return s;
    };
    a = covertToString(a);
    b = covertToString(b);
    return a === b;
  }

}

module.exports = Json;