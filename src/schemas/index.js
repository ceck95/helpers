module.exports = {
  error: require('./error'),
  pagination: require('./pagination'),
  oAuth: require('./oauth')
};
