const Joi = require('joi');

module.exports = [{
  code: Joi.string(),
  message: Joi.string(),
  messageUi: Joi.string(),
  source: Joi.string()
}];