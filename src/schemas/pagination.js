const Joi = require('joi');

module.exports = {
  query: {
    pageSize: Joi.number(),
    pageNumber: Joi.number().min(1).required()
  }
};