const nodemailer = require('nodemailer');
const config = require('config');
const Hoek = require('hoek');
const BPromise = require('bluebird');
const messageEmptyConfig = 'Empty config email';
const pug = require('pug');
const fs = require('fs');
const aws = require('aws-sdk');

class Email {

  static get transporter() {
    Hoek.assert(config.email, messageEmptyConfig);
    return nodemailer.createTransport(config.email.transporter, {
      service: config.email.service,
      auth: {
        user: config.email.user,
        pass: config.email.pass
      }
    });
  }

  static parseBody(body) {
    let isHtml = false;
    if (typeof body === 'object') {
      const pathTemplate = `${process.cwd()}/templates/email/${body.fileName}`;
      if (!fs.existsSync(pathTemplate))
        Hoek.assert(false, `Directory include templates email not correct , please you flow structure templates/email/${body.fileName}`);
      body = pug.renderFile(pathTemplate, body.params);
      isHtml = true;
    }
    return {
      content: body,
      isHtml: isHtml
    };
  }

  static sendMail(req, to, subject, body) {
    Hoek.assert(config.email, messageEmptyConfig);
    req.log.info('Sent email starting ...');
    return new BPromise((resolve, reject) => {
      body = Email.parseBody(body).content;
      const mailOptions = {
        from: config.email.user,
        to: Array.isArray(to) ? to.join(',') : to,
        subject: subject,
        html: body
      };
      return Email.transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
          req.log.error(`Sent email error ${error.toString()}`);
          return reject(error);
        }
        req.log.info('Sent email successfully');
        return resolve(info.response);
      });
    });
  }

  static parseBodyAWS(body) {
    body = Email.parseBody(body);
    return {
      [body.isHtml ? 'Html' : 'Text']: {
        Data: body.content,
        Charset: 'utf8'
      }
    };
  }

  static sendMailAWS(req, to, subject, source, body) {
    Hoek.assert(config.email.aws, messageEmptyConfig);
    const configAws = config.email.aws;
    aws.config.accessKeyId = configAws.accessKeyId;
    aws.config.secretAccessKey = configAws.secretAccessKey;
    aws.config.region = configAws.region;
    aws.config.apiVersion = 'latest';
    const ses = new aws.SES();
    req.log.info('Sent email AWS starting ...');
    return new BPromise((resolve, reject) => {
      const settings = {
        Destination: {
          ToAddresses: Array.isArray(to) ? to : [to]
        },
        Message: {
          Subject: {
            Data: subject,
            Charset: 'utf8'
          },
          Body: Email.parseBodyAWS(body)
        },
        Source: source,
        ReplyToAddresses: []
      };
      return ses.sendEmail(settings, (err, data) => {
        if (err) {
          req.log.fatal(`Sent email AWS error ${err.toString()}`);
          return reject(err);
        }
        req.log.info('Sent email AWS successfully');
        return resolve(data);
      });
    });
  }

}

module.exports = Email;